﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp3_lesson15
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            updateCategoryDropdown();
            decimal bal = Convert.ToDecimal(registerTableAdapter.GetBalance());
            balancetoolStripStatusLabel.Text = "Balance - $" + bal;
            
        }

        private void updateCategoryDropdown()
        {
            string displayMember = categoryComboBox.DisplayMember;
            string valueMember = categoryComboBox.ValueMember;
            categoryComboBox.DataSource = null;
            this.categoriesTableAdapter.Fill(this.mymoneyDataSet.Categories);
            categoryComboBox.DataSource = categoriesBindingSource;
            categoryComboBox.DisplayMember = displayMember;
            categoryComboBox.ValueMember = valueMember;

        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mymoneyDataSet.CategoryBreakdown' table. You can move, or remove it, as needed.
            //this.categoryBreakdownTableAdapter.Fill(this.mymoneyDataSet.CategoryBreakdown);
            // TODO: This line of code loads data into the 'mymoneyDataSet.Categories' table. You can move, or remove it, as needed.
            this.categoriesTableAdapter.Fill(this.mymoneyDataSet.Categories);
            // TODO: This line of code loads data into the 'mymoneyDataSet.Register' table. You can move, or remove it, as needed.
            this.registerTableAdapter.Fill(this.mymoneyDataSet.Register);
            registerCategoryTableAdapter.Fill(mymoneyDataSet.RegisterCategory);

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void categoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CategoryBreakdown cb = new CategoryBreakdown();
            cb.ShowDialog();
        }

        private void categoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (CategoriesForm categoriesForm = new CategoriesForm())
            {
                categoriesForm.ShowDialog();
            }
            updateCategoryDropdown();

        }

        private void addbutton_Click(object sender, EventArgs e)
        {
            // Get category
            int categoryID = Convert.ToInt32(categoryComboBox.SelectedValue);

            // Get amount
            decimal amount = 0.0m;
            decimal.TryParse(amountTextBox.Text, out amount);

            // Get date
            DateTime date;
            DateTime.TryParse(dateTimePicker.Value.ToShortDateString(), out date);
            // Determine if category is a debit, and adjust amount sign accordingly
            bool? isDebit = (bool) categoriesTableAdapter.isCategoryDebit(categoryID);
            if (!isDebit.HasValue) isDebit = false;
            amount = Math.Abs(amount);
            if (isDebit.Value == true) amount *= -1.0m;
            try
            {
                // Perform insert
                registerTableAdapter.Insert(
                        categoryID,
                        string.IsNullOrWhiteSpace(identifierTextBox.Text) ? null : identifierTextBox.Text,
                        string.IsNullOrWhiteSpace(descriptionTextBox.Text) ? null : descriptionTextBox.Text,
                        string.IsNullOrWhiteSpace(noteTextBox.Text) ? null : noteTextBox.Text,
                        amount,
                        date);


                // Refresh dataset from database
                registerCategoryTableAdapter.Fill(mymoneyDataSet.RegisterCategory);

                // Clear add controls
                identifierTextBox.Text = "";
                descriptionTextBox.Text = "";
                amountTextBox.Text = "";
                noteTextBox.Text = "";
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show(ex.Message, "Null Value");
            }

        }

        private void categoriesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CategoriesForm c = new CategoriesForm();
            c.ShowDialog();
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void categoriesToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CategoryBreakdown cb = new CategoryBreakdown();
            cb.ShowDialog();
        }

        private void moneydataGridView_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (e.RowIndex >= 0)
                    moneydataGridView.CurrentCell = moneydataGridView.Rows[e.RowIndex].Cells[1];

                Point mousePoint = moneydataGridView.PointToClient(Cursor.Position);
                DataGridView.HitTestInfo hitTestInfo = moneydataGridView.HitTest(mousePoint.X, mousePoint.Y);
                if (hitTestInfo.Type == DataGridViewHitTestType.Cell)
                    registercontextMenuStrip.Show(moneydataGridView, mousePoint);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (moneydataGridView.CurrentRow.Index >= 0)
            {
                string registerIDString = moneydataGridView.Rows[moneydataGridView.CurrentRow.Index].Cells[0].Value.ToString();
                int registerID = -1;
                if (int.TryParse(registerIDString, out registerID))
                {
                    if (DialogResult.Yes == MessageBox.Show("Delete row " + moneydataGridView.CurrentRow.Index + " ?", "Delete Entry", MessageBoxButtons.YesNo))
                    {
                        registerTableAdapter.Delete(registerID);
                        registerCategoryTableAdapter.Fill(mymoneyDataSet.RegisterCategory);                        
                    }
                }
            }

        }

    }
}
