﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp3_lesson15
{
    public partial class CategoryBreakdown : Form
    {
        public CategoryBreakdown()
        {
            InitializeComponent();
        }

        private void CategoryBreakdown_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mymoneyDataSet.CategoryBreakdown' table. You can move, or remove it, as needed.
            // TODO: This line of code loads data into the 'mymoneyDataSet.Categories' table. You can move, or remove it, as needed.
            this.categoriesTableAdapter.Fill(this.mymoneyDataSet.Categories);
            // Set the DateTimePicker initial dates (1st and last day of the current month/year)
            DateTime firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            startDateTimePicker.Value = firstDayOfMonth;
            endDateTimePicker.Value = lastDayOfMonth;

            // Update the report
            updateReport();

        }

        private void updateReport()
        {
            categoryBreakdownTableAdapter.Fill(this.mymoneyDataSet.CategoryBreakdown, startDateTimePicker.Value, endDateTimePicker.Value);
        }


        private void closebutton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            string date = startDateTimePicker.Text;
            updatereport();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            string date = startDateTimePicker.Text;
            updatereport();
        }

        private void updatereport()
        {
            if (!String.IsNullOrEmpty(startDateTimePicker.Text) && !String.IsNullOrEmpty(endDateTimePicker.Text))
            {
                DateTime dt1 = Convert.ToDateTime(startDateTimePicker.Text);
                DateTime dt2 = Convert.ToDateTime(endDateTimePicker.Text);
                categoryBreakdownTableAdapter.Fill(mymoneyDataSet.CategoryBreakdown, dt1, dt2);
            }
        }
    }
}
