﻿namespace csharp3_lesson15
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label registerDateTimeLabel;
            System.Windows.Forms.Label registerIdentifierLabel;
            System.Windows.Forms.Label registerDescriptionLabel;
            System.Windows.Forms.Label registerAmountLabel;
            System.Windows.Forms.Label registerNoteLabel;
            System.Windows.Forms.Label categoryNameLabel;
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.registerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mymoneyDataSet = new csharp3_lesson15.mymoneyDataSet();
            this.identifierTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.noteTextBox = new System.Windows.Forms.TextBox();
            this.addbutton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.balancetoolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.moneydataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.registerCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoryBreakdownBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableAdapterManager = new csharp3_lesson15.mymoneyDataSetTableAdapters.TableAdapterManager();
            this.categoriesTableAdapter = new csharp3_lesson15.mymoneyDataSetTableAdapters.CategoriesTableAdapter();
            this.registerCategoryTableAdapter = new csharp3_lesson15.mymoneyDataSetTableAdapters.RegisterCategoryTableAdapter();
            this.categoryBreakdownTableAdapter = new csharp3_lesson15.mymoneyDataSetTableAdapters.CategoryBreakdownTableAdapter();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.categoryComboBox = new System.Windows.Forms.ComboBox();
            this.registercontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerTableAdapter = new csharp3_lesson15.mymoneyDataSetTableAdapters.RegisterTableAdapter();
            registerDateTimeLabel = new System.Windows.Forms.Label();
            registerIdentifierLabel = new System.Windows.Forms.Label();
            registerDescriptionLabel = new System.Windows.Forms.Label();
            registerAmountLabel = new System.Windows.Forms.Label();
            registerNoteLabel = new System.Windows.Forms.Label();
            categoryNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.registerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mymoneyDataSet)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moneydataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBreakdownBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.registercontextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // registerDateTimeLabel
            // 
            registerDateTimeLabel.AutoSize = true;
            registerDateTimeLabel.Location = new System.Drawing.Point(18, 8);
            registerDateTimeLabel.Name = "registerDateTimeLabel";
            registerDateTimeLabel.Size = new System.Drawing.Size(30, 13);
            registerDateTimeLabel.TabIndex = 2;
            registerDateTimeLabel.Text = "Date";
            // 
            // registerIdentifierLabel
            // 
            registerIdentifierLabel.AutoSize = true;
            registerIdentifierLabel.Location = new System.Drawing.Point(221, 8);
            registerIdentifierLabel.Name = "registerIdentifierLabel";
            registerIdentifierLabel.Size = new System.Drawing.Size(18, 13);
            registerIdentifierLabel.TabIndex = 4;
            registerIdentifierLabel.Text = "ID";
            // 
            // registerDescriptionLabel
            // 
            registerDescriptionLabel.AutoSize = true;
            registerDescriptionLabel.Location = new System.Drawing.Point(316, 8);
            registerDescriptionLabel.Name = "registerDescriptionLabel";
            registerDescriptionLabel.Size = new System.Drawing.Size(60, 13);
            registerDescriptionLabel.TabIndex = 6;
            registerDescriptionLabel.Text = "Description";
            // 
            // registerAmountLabel
            // 
            registerAmountLabel.AutoSize = true;
            registerAmountLabel.Location = new System.Drawing.Point(432, 8);
            registerAmountLabel.Name = "registerAmountLabel";
            registerAmountLabel.Size = new System.Drawing.Size(43, 13);
            registerAmountLabel.TabIndex = 8;
            registerAmountLabel.Text = "Amount";
            // 
            // registerNoteLabel
            // 
            registerNoteLabel.AutoSize = true;
            registerNoteLabel.Location = new System.Drawing.Point(264, 64);
            registerNoteLabel.Name = "registerNoteLabel";
            registerNoteLabel.Size = new System.Drawing.Size(30, 13);
            registerNoteLabel.TabIndex = 10;
            registerNoteLabel.Text = "Note";
            // 
            // categoryNameLabel
            // 
            categoryNameLabel.AutoSize = true;
            categoryNameLabel.Location = new System.Drawing.Point(557, 8);
            categoryNameLabel.Name = "categoryNameLabel";
            categoryNameLabel.Size = new System.Drawing.Size(49, 13);
            categoryNameLabel.TabIndex = 18;
            categoryNameLabel.Text = "Category";
            // 
            // menuStrip
            // 
            this.menuStrip.Location = new System.Drawing.Point(0, 24);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(826, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.categoriesToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // categoriesToolStripMenuItem
            // 
            this.categoriesToolStripMenuItem.Name = "categoriesToolStripMenuItem";
            this.categoriesToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.categoriesToolStripMenuItem.Text = "Categories";
            this.categoriesToolStripMenuItem.Click += new System.EventHandler(this.categoriesToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.categoryToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // categoryToolStripMenuItem
            // 
            this.categoryToolStripMenuItem.Name = "categoryToolStripMenuItem";
            this.categoryToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.categoryToolStripMenuItem.Text = "Category";
            this.categoryToolStripMenuItem.Click += new System.EventHandler(this.categoryToolStripMenuItem_Click);
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.registerBindingSource, "registerDateTime", true));
            this.dateTimePicker.Location = new System.Drawing.Point(18, 32);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker.TabIndex = 3;
            // 
            // registerBindingSource
            // 
            this.registerBindingSource.DataMember = "Register";
            this.registerBindingSource.DataSource = this.mymoneyDataSet;
            // 
            // mymoneyDataSet
            // 
            this.mymoneyDataSet.DataSetName = "mymoneyDataSet";
            this.mymoneyDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // identifierTextBox
            // 
            this.identifierTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.registerBindingSource, "registerIdentifier", true));
            this.identifierTextBox.Location = new System.Drawing.Point(224, 31);
            this.identifierTextBox.Name = "identifierTextBox";
            this.identifierTextBox.Size = new System.Drawing.Size(73, 20);
            this.identifierTextBox.TabIndex = 5;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.registerBindingSource, "registerDescription", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(319, 33);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.descriptionTextBox.TabIndex = 7;
            // 
            // amountTextBox
            // 
            this.amountTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.registerBindingSource, "registerAmount", true));
            this.amountTextBox.Location = new System.Drawing.Point(435, 34);
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.Size = new System.Drawing.Size(100, 20);
            this.amountTextBox.TabIndex = 9;
            // 
            // noteTextBox
            // 
            this.noteTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.registerBindingSource, "registerNote", true));
            this.noteTextBox.Location = new System.Drawing.Point(319, 61);
            this.noteTextBox.Multiline = true;
            this.noteTextBox.Name = "noteTextBox";
            this.noteTextBox.Size = new System.Drawing.Size(362, 54);
            this.noteTextBox.TabIndex = 11;
            // 
            // addbutton
            // 
            this.addbutton.Location = new System.Drawing.Point(696, 31);
            this.addbutton.Name = "addbutton";
            this.addbutton.Size = new System.Drawing.Size(75, 23);
            this.addbutton.TabIndex = 12;
            this.addbutton.Text = "Add";
            this.addbutton.UseVisualStyleBackColor = true;
            this.addbutton.Click += new System.EventHandler(this.addbutton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.balancetoolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 517);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(826, 22);
            this.statusStrip1.TabIndex = 13;
            this.statusStrip1.Text = "Balance";
            // 
            // balancetoolStripStatusLabel
            // 
            this.balancetoolStripStatusLabel.Name = "balancetoolStripStatusLabel";
            this.balancetoolStripStatusLabel.Size = new System.Drawing.Size(48, 17);
            this.balancetoolStripStatusLabel.Text = "Balance";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.reportsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(562, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem1,
            this.reportsToolStripMenuItem1});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(826, 24);
            this.menuStrip2.TabIndex = 16;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // fileToolStripMenuItem1
            // 
            this.fileToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.categoriesToolStripMenuItem1,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
            this.fileToolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem1.Text = "File";
            // 
            // categoriesToolStripMenuItem1
            // 
            this.categoriesToolStripMenuItem1.Name = "categoriesToolStripMenuItem1";
            this.categoriesToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.categoriesToolStripMenuItem1.Text = "Categories";
            this.categoriesToolStripMenuItem1.Click += new System.EventHandler(this.categoriesToolStripMenuItem1_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // reportsToolStripMenuItem1
            // 
            this.reportsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.categoriesToolStripMenuItem2});
            this.reportsToolStripMenuItem1.Name = "reportsToolStripMenuItem1";
            this.reportsToolStripMenuItem1.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem1.Text = "Reports";
            // 
            // categoriesToolStripMenuItem2
            // 
            this.categoriesToolStripMenuItem2.Name = "categoriesToolStripMenuItem2";
            this.categoriesToolStripMenuItem2.Size = new System.Drawing.Size(130, 22);
            this.categoriesToolStripMenuItem2.Text = "Categories";
            this.categoriesToolStripMenuItem2.Click += new System.EventHandler(this.categoriesToolStripMenuItem2_Click);
            // 
            // moneydataGridView
            // 
            this.moneydataGridView.AutoGenerateColumns = false;
            this.moneydataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.moneydataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewCheckBoxColumn1});
            this.moneydataGridView.DataSource = this.registerCategoryBindingSource;
            this.moneydataGridView.Location = new System.Drawing.Point(0, 24);
            this.moneydataGridView.Name = "moneydataGridView";
            this.moneydataGridView.Size = new System.Drawing.Size(823, 354);
            this.moneydataGridView.TabIndex = 18;
            this.moneydataGridView.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.moneydataGridView_CellMouseDown);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "registerID";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "categoryName";
            this.dataGridViewTextBoxColumn7.HeaderText = "Category Name";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "registerDateTime";
            this.dataGridViewTextBoxColumn2.HeaderText = "Date Time";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "registerIdentifier";
            this.dataGridViewTextBoxColumn3.HeaderText = "Identifier";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "registerAmount";
            this.dataGridViewTextBoxColumn4.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "registerDescription";
            this.dataGridViewTextBoxColumn5.HeaderText = "Description";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "registerNote";
            this.dataGridViewTextBoxColumn6.HeaderText = "Note";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "isDebit";
            this.dataGridViewCheckBoxColumn1.HeaderText = "isDebit";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // registerCategoryBindingSource
            // 
            this.registerCategoryBindingSource.DataMember = "RegisterCategory";
            this.registerCategoryBindingSource.DataSource = this.mymoneyDataSet;
            // 
            // categoriesBindingSource
            // 
            this.categoriesBindingSource.DataMember = "Categories";
            this.categoriesBindingSource.DataSource = this.mymoneyDataSet;
            // 
            // categoryBreakdownBindingSource
            // 
            this.categoryBreakdownBindingSource.DataMember = "CategoryBreakdown";
            this.categoryBreakdownBindingSource.DataSource = this.mymoneyDataSet;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CategoriesHitsTableAdapter = null;
            this.tableAdapterManager.CategoriesTableAdapter = this.categoriesTableAdapter;
            this.tableAdapterManager.RegisterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = csharp3_lesson15.mymoneyDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // categoriesTableAdapter
            // 
            this.categoriesTableAdapter.ClearBeforeFill = true;
            // 
            // registerCategoryTableAdapter
            // 
            this.registerCategoryTableAdapter.ClearBeforeFill = true;
            // 
            // categoryBreakdownTableAdapter
            // 
            this.categoryBreakdownTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.categoryComboBox);
            this.splitContainer1.Panel2.Controls.Add(categoryNameLabel);
            this.splitContainer1.Panel2.Controls.Add(this.addbutton);
            this.splitContainer1.Panel2.Controls.Add(registerIdentifierLabel);
            this.splitContainer1.Panel2.Controls.Add(this.noteTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.identifierTextBox);
            this.splitContainer1.Panel2.Controls.Add(registerNoteLabel);
            this.splitContainer1.Panel2.Controls.Add(registerDateTimeLabel);
            this.splitContainer1.Panel2.Controls.Add(registerAmountLabel);
            this.splitContainer1.Panel2.Controls.Add(this.dateTimePicker);
            this.splitContainer1.Panel2.Controls.Add(this.descriptionTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.amountTextBox);
            this.splitContainer1.Panel2.Controls.Add(registerDescriptionLabel);
            this.splitContainer1.Size = new System.Drawing.Size(826, 539);
            this.splitContainer1.SplitterDistance = 381;
            this.splitContainer1.TabIndex = 20;
            // 
            // categoryComboBox
            // 
            this.categoryComboBox.DataSource = this.categoriesBindingSource;
            this.categoryComboBox.DisplayMember = "categoryName";
            this.categoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.categoryComboBox.FormattingEnabled = true;
            this.categoryComboBox.Location = new System.Drawing.Point(560, 34);
            this.categoryComboBox.Name = "categoryComboBox";
            this.categoryComboBox.Size = new System.Drawing.Size(121, 21);
            this.categoryComboBox.TabIndex = 0;
            this.categoryComboBox.ValueMember = "categoryID";
            // 
            // registercontextMenuStrip
            // 
            this.registercontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.registercontextMenuStrip.Name = "registercontextMenuStrip";
            this.registercontextMenuStrip.Size = new System.Drawing.Size(108, 26);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // registerTableAdapter
            // 
            this.registerTableAdapter.ClearBeforeFill = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 539);
            this.Controls.Add(this.moneydataGridView);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.splitContainer1);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "My Money";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.registerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mymoneyDataSet)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moneydataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBreakdownBindingSource)).EndInit();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.registercontextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoryToolStripMenuItem;
        private mymoneyDataSet mymoneyDataSet;
        private System.Windows.Forms.BindingSource registerBindingSource;
        private mymoneyDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.TextBox identifierTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox amountTextBox;
        private System.Windows.Forms.TextBox noteTextBox;
        private System.Windows.Forms.Button addbutton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel balancetoolStripStatusLabel;
        private mymoneyDataSetTableAdapters.CategoriesTableAdapter categoriesTableAdapter;
        private System.Windows.Forms.BindingSource categoriesBindingSource;
        private mymoneyDataSetTableAdapters.RegisterCategoryTableAdapter registerCategoryTableAdapter;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.BindingSource categoryBreakdownBindingSource;
        private mymoneyDataSetTableAdapters.CategoryBreakdownTableAdapter categoryBreakdownTableAdapter;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem categoriesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem categoriesToolStripMenuItem2;
        private System.Windows.Forms.BindingSource registerCategoryBindingSource;
        private System.Windows.Forms.DataGridView moneydataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ContextMenuStrip registercontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private mymoneyDataSetTableAdapters.RegisterTableAdapter registerTableAdapter;
        private System.Windows.Forms.ComboBox categoryComboBox;
    }
}

