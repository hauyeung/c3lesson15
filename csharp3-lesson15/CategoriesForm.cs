﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp3_lesson15
{
    public partial class CategoriesForm : Form
    {
        public CategoriesForm()
        {
            InitializeComponent();

        }

        private void Categories_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mymoneyDataSet.CategortHits' table. You can move, or remove it, as needed.
            this.categoriesHitsTableAdapter.Fill(this.mymoneyDataSet.CategoriesHits);
            // TODO: This line of code loads data into the 'mymoneyDataSet.Categories' table. You can move, or remove it, as needed.
            this.categoriesTableAdapter.Fill(this.mymoneyDataSet.Categories);

        }

        private void closebutton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void savetoolStripButton_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.categoriesBindingSource.EndEdit();
            categoriesTableAdapter.Update(mymoneyDataSet.Categories);
            categoriesHitsTableAdapter.Update(mymoneyDataSet.CategoriesHits);
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (categoriesHitsDataGridView.CurrentRow.Index >= 0)
            {
                string categoryName = categoriesHitsDataGridView.CurrentRow.Cells[1].Value.ToString();
                int hits = 0;
                int.TryParse(categoriesHitsDataGridView.CurrentRow.Cells[4].Value.ToString(), out hits);

                if (hits > 0)
                    MessageBox.Show(categoryName + " category is in use and may not be deleted.", "Delete Category");
                else
                {
                    if (DialogResult.Yes == MessageBox.Show("Delete " + categoryName + "?", "Delete Category", MessageBoxButtons.YesNo))
                    {
                        categoriesHitsBindingSource.RemoveCurrent();
                        categoriesHitsTableAdapter.Update(mymoneyDataSet.CategoriesHits);
                    }
                }
            }

        
        }


    }
}
