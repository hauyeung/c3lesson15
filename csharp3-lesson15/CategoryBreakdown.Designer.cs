﻿namespace csharp3_lesson15
{
    partial class CategoryBreakdown
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mymoneyDataSet = new csharp3_lesson15.mymoneyDataSet();
            this.categoriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoriesTableAdapter = new csharp3_lesson15.mymoneyDataSetTableAdapters.CategoriesTableAdapter();
            this.categoryBreakdownBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoryBreakdownTableAdapter = new csharp3_lesson15.mymoneyDataSetTableAdapters.CategoryBreakdownTableAdapter();
            this.startDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.endDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.closebutton = new System.Windows.Forms.Button();
            this.cbdataGridView = new System.Windows.Forms.DataGridView();
            this.categoryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.enddatelabel = new System.Windows.Forms.Label();
            this.startdatelabel = new System.Windows.Forms.Label();
            this.categoryBreakdownTableAdapter1 = new csharp3_lesson15.mymoneyDataSetTableAdapters.CategoryBreakdownTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.mymoneyDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBreakdownBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbdataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mymoneyDataSet
            // 
            this.mymoneyDataSet.DataSetName = "mymoneyDataSet";
            this.mymoneyDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // categoriesBindingSource
            // 
            this.categoriesBindingSource.DataMember = "Categories";
            this.categoriesBindingSource.DataSource = this.mymoneyDataSet;
            // 
            // categoriesTableAdapter
            // 
            this.categoriesTableAdapter.ClearBeforeFill = true;
            // 
            // categoryBreakdownBindingSource
            // 
            this.categoryBreakdownBindingSource.DataMember = "CategoryBreakdown";
            this.categoryBreakdownBindingSource.DataSource = this.mymoneyDataSet;
            // 
            // categoryBreakdownTableAdapter
            // 
            this.categoryBreakdownTableAdapter.ClearBeforeFill = true;
            // 
            // startDateTimePicker
            // 
            this.startDateTimePicker.Location = new System.Drawing.Point(27, 324);
            this.startDateTimePicker.Name = "startDateTimePicker";
            this.startDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.startDateTimePicker.TabIndex = 1;
            this.startDateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // endDateTimePicker
            // 
            this.endDateTimePicker.Location = new System.Drawing.Point(273, 324);
            this.endDateTimePicker.Name = "endDateTimePicker";
            this.endDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.endDateTimePicker.TabIndex = 2;
            this.endDateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // closebutton
            // 
            this.closebutton.Location = new System.Drawing.Point(213, 13);
            this.closebutton.Name = "closebutton";
            this.closebutton.Size = new System.Drawing.Size(75, 23);
            this.closebutton.TabIndex = 3;
            this.closebutton.Text = "Close";
            this.closebutton.UseVisualStyleBackColor = true;
            this.closebutton.Click += new System.EventHandler(this.closebutton_Click);
            // 
            // cbdataGridView
            // 
            this.cbdataGridView.AutoGenerateColumns = false;
            this.cbdataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cbdataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.categoryDataGridViewTextBoxColumn,
            this.amountDataGridViewTextBoxColumn});
            this.cbdataGridView.DataSource = this.categoryBreakdownBindingSource;
            this.cbdataGridView.Location = new System.Drawing.Point(27, 22);
            this.cbdataGridView.Name = "cbdataGridView";
            this.cbdataGridView.Size = new System.Drawing.Size(432, 258);
            this.cbdataGridView.TabIndex = 4;
            // 
            // categoryDataGridViewTextBoxColumn
            // 
            this.categoryDataGridViewTextBoxColumn.DataPropertyName = "Category";
            this.categoryDataGridViewTextBoxColumn.HeaderText = "Category";
            this.categoryDataGridViewTextBoxColumn.Name = "categoryDataGridViewTextBoxColumn";
            // 
            // amountDataGridViewTextBoxColumn
            // 
            this.amountDataGridViewTextBoxColumn.DataPropertyName = "Amount";
            this.amountDataGridViewTextBoxColumn.HeaderText = "Amount";
            this.amountDataGridViewTextBoxColumn.Name = "amountDataGridViewTextBoxColumn";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, 2);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.enddatelabel);
            this.splitContainer1.Panel1.Controls.Add(this.startdatelabel);
            this.splitContainer1.Panel1.Controls.Add(this.startDateTimePicker);
            this.splitContainer1.Panel1.Controls.Add(this.endDateTimePicker);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.closebutton);
            this.splitContainer1.Size = new System.Drawing.Size(495, 412);
            this.splitContainer1.SplitterDistance = 356;
            this.splitContainer1.TabIndex = 5;
            // 
            // enddatelabel
            // 
            this.enddatelabel.AutoSize = true;
            this.enddatelabel.Location = new System.Drawing.Point(270, 285);
            this.enddatelabel.Name = "enddatelabel";
            this.enddatelabel.Size = new System.Drawing.Size(52, 13);
            this.enddatelabel.TabIndex = 4;
            this.enddatelabel.Text = "End Date";
            // 
            // startdatelabel
            // 
            this.startdatelabel.AutoSize = true;
            this.startdatelabel.Location = new System.Drawing.Point(24, 285);
            this.startdatelabel.Name = "startdatelabel";
            this.startdatelabel.Size = new System.Drawing.Size(55, 13);
            this.startdatelabel.TabIndex = 3;
            this.startdatelabel.Text = "Start Date";
            // 
            // categoryBreakdownTableAdapter1
            // 
            this.categoryBreakdownTableAdapter1.ClearBeforeFill = true;
            // 
            // CategoryBreakdown
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 410);
            this.Controls.Add(this.cbdataGridView);
            this.Controls.Add(this.splitContainer1);
            this.Name = "CategoryBreakdown";
            this.Text = "Category Breakdown";
            this.Load += new System.EventHandler(this.CategoryBreakdown_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mymoneyDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBreakdownBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbdataGridView)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private mymoneyDataSet mymoneyDataSet;
        private System.Windows.Forms.BindingSource categoriesBindingSource;
        private mymoneyDataSetTableAdapters.CategoriesTableAdapter categoriesTableAdapter;
        private System.Windows.Forms.BindingSource categoryBreakdownBindingSource;
        private mymoneyDataSetTableAdapters.CategoryBreakdownTableAdapter categoryBreakdownTableAdapter;
        private System.Windows.Forms.DateTimePicker startDateTimePicker;
        private System.Windows.Forms.DateTimePicker endDateTimePicker;
        private System.Windows.Forms.Button closebutton;
        private System.Windows.Forms.DataGridView cbdataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountDataGridViewTextBoxColumn;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label enddatelabel;
        private System.Windows.Forms.Label startdatelabel;
        private mymoneyDataSetTableAdapters.CategoryBreakdownTableAdapter categoryBreakdownTableAdapter1;

    }
}